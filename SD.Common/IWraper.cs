﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;

namespace SD.Common
{
    /// <summary>
    /// 
    /// </summary>
    public interface IWrapper
    {
        /// <summary>
        /// 
        /// </summary>
        bool Enabled
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="bs"></param>
        /// <returns></returns>
        byte[] Wrap(byte[] bs);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="bs"></param>
        /// <returns></returns>
        byte[] Unwrap(byte[] bs);

        /// <summary>
        /// 
        /// </summary>
        string Description
        {
            get;
            set;
        }
    }

    public interface ICrc
    {
        string Name
        {
            get;
        }

        string Description
        {
            get;
        }

        byte[] Calc(byte[] bytes, int begin, int length);
    }

}
