using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SD.Common;

namespace SocketClient
{

    public class CRCXor : ICrc
    {
        public string Name
        {
            get
            {
                return "CRCXor";
            }
        }

        public string Description
        {
            get
            {
                return "���У�������";
            }
        }

        public byte[] Calc(byte[] bytes, int begin, int length)
        {
            int num = 0;
            for (int i = 0; i < bytes.Length; i++)
            {
                byte b = bytes[i];
                num ^= (int)b;
            }
            num %= 256;
            return new byte[]
            {
                checked((byte)num)
            };
        }
    }
}
