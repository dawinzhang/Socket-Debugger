using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SD.Common;

namespace SocketClient
{

    public class CRC16 : ICrc
    {
        public string Name
        {
            get
            {
                return "CRC16";
            }
        }

        public string Description
        {
            get
            {
                return "CRC16 ������";
            }
        }

        public static void CalculateCRC(byte[] pByte, int nBeginIndex, int nNumberOfBytes, out ushort pChecksum)
        {
            pChecksum = 65535;
            int i = nBeginIndex;
            int num = i + nNumberOfBytes;
            while (i < num)
            {
                pChecksum ^= (ushort)pByte[i];
                for (int j = 0; j < 8; j++)
                {
                    ushort num2;
                    if ((pChecksum & 1) == 1)
                    {
                        num2 = 1;
                    }
                    else
                    {
                        num2 = 0;
                    }
                    pChecksum = (ushort)(pChecksum >> 1);
                    if (num2 != 0)
                    {
                        pChecksum ^= 40961;
                    }
                }
                i++;
            }
        }

        public static byte[] CalculateCRC(byte[] pByte, int nBeginIndex, int nNumberOfBytes)
        {
            ushort value;
            CRC16.CalculateCRC(pByte, nBeginIndex, nNumberOfBytes, out value);
            return BitConverter.GetBytes(value);
        }

        public static byte[] CalculateCRC(byte[] pByte)
        {
            return CRC16.CalculateCRC(pByte, 0, pByte.Length);
        }

        public byte[] Calc(byte[] bytes, int begin, int length)
        {
            return CRC16.CalculateCRC(bytes, begin, length);
        }
    }
}
